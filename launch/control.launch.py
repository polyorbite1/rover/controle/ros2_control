import launch
import launch_ros.actions
from launch.substitutions import LaunchConfiguration
from launch.actions import DeclareLaunchArgument

def generate_launch_description():
    # Declare a launch argument to specify the parameter value
    # my_param = DeclareLaunchArgument('params-file', default_value='/home/polyorbite/Projects/polyorbite/rover/ros2_ws/src/ros2_control/config/camera.yaml')

    return launch.LaunchDescription(
        [
            # TODO: remove, only for testing 
            # my_param,
            launch_ros.actions.Node(
                package='usb_cam',
                executable='usb_cam_node_exe',
                name='usb_cam_node',
                parameters=[
                    '/home/polyorbite/Projects/polyorbite/rover/ros2_ws/src/ros2_control/config/camera.yaml'
                ], 
                remappings = [
                    ('/image_raw', '/camera/image_raw'),
                    ('/camera_info', '/camera/camera_info')
                ]
            ),
            launch_ros.actions.Node(
                package='ros2_control',
                executable='ros2_control_node',
                name='ros2_control_node'
            ),
            launch_ros.actions.Node(
                package='ros1_bridge',
                executable='dynamic_bridge',
                name='dynamic_bridge'
            ),
            launch_ros.actions.Node(
                package='rosbridge_server',
                executable='rosbridge_websocket',
                name='rosbridge_websocket'
            ),
        ]
    )