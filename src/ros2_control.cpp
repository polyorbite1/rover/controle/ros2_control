#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include "rclcpp/rclcpp.hpp"
// #include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/int8.hpp"
#include "std_msgs/msg/u_int8.hpp"
#include "std_msgs/msg/bool.hpp"
#include "geometry_msgs/msg/twist.hpp"
using std::placeholders::_1;

class Mega : public rclcpp::Node {
public:
    Mega() : Node("mega_bridge") {
        // switch topics
        blue_publisher_ = this->create_publisher<std_msgs::msg::Bool>("control/mega/switch/blue/state", 10);
        red_publisher_ = this->create_publisher<std_msgs::msg::Bool>("control/mega/switch/red/state", 10);
        green_publisher_ = this->create_publisher<std_msgs::msg::Bool>("control/mega/switch/green/state", 10);
        orange_publisher_ = this->create_publisher<std_msgs::msg::Bool>("control/mega/switch/orange/state", 10);
        buzzer_publisher_ = this->create_publisher<std_msgs::msg::Bool>("control/mega/switch/buzzer/state", 10);
        light_publisher_ = this->create_publisher<std_msgs::msg::Bool>("control/mega/switch/light/state", 10);

        // fan topics
        intake1_publisher_ = this->create_publisher<std_msgs::msg::UInt8>("control/mega/fan/intake1/velocity", 10);
        intake2_publisher_ = this->create_publisher<std_msgs::msg::UInt8>("control/mega/fan/intake2/velocity", 10);
        outake1_publisher_ = this->create_publisher<std_msgs::msg::UInt8>("control/mega/fan/outake1/velocity", 10);
        outake2_publisher_ = this->create_publisher<std_msgs::msg::UInt8>("control/mega/fan/outake2/velocity", 10);

        // motor topics
        FR_velocity_publisher_ = this->create_publisher<std_msgs::msg::Int8>("control/mega/motor/FR/speed", 10);
        FL_velocity_publisher_ = this->create_publisher<std_msgs::msg::Int8>("control/mega/motor/FL/speed", 10);
        CR_velocity_publisher_ = this->create_publisher<std_msgs::msg::Int8>("control/mega/motor/CR/speed", 10);
        CL_velocity_publisher_ = this->create_publisher<std_msgs::msg::Int8>("control/mega/motor/CL/speed", 10);
        RR_velocity_publisher_ = this->create_publisher<std_msgs::msg::Int8>("control/mega/motor/RR/speed", 10);
        RL_velocity_publisher_ = this->create_publisher<std_msgs::msg::Int8>("control/mega/motor/RL/speed", 10);

        right_velocity_subscriber_ = this->create_subscription<geometry_msgs::msg::Twist>(
                "right_velocity", 10, std::bind(&Mega::rightDrivers, this, _1));
        left_velocity_subscriber_ = this->create_subscription<geometry_msgs::msg::Twist>(
                "left_velocity", 10, std::bind(&Mega::leftDrivers, this, _1));


    }

private:

    void leftDrivers(const geometry_msgs::msg::Twist::SharedPtr msg) const {
        auto speed = std_msgs::msg::Int8();
        speed.data = (int8_t)(msg->linear.x * 127 / 40) / 2;
        FL_velocity_publisher_->publish(speed);
        CL_velocity_publisher_->publish(speed);
        RL_velocity_publisher_->publish(speed);
    }

    void rightDrivers(const geometry_msgs::msg::Twist::SharedPtr msg) const {
        auto speed = std_msgs::msg::Int8();
        speed.data = (int8_t)(msg->linear.x * 127 / 40) / 2;
        FR_velocity_publisher_->publish(speed);
        CR_velocity_publisher_->publish(speed);
        RR_velocity_publisher_->publish(speed);
    }

    // switch topics
    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr blue_publisher_;
    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr red_publisher_;
    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr green_publisher_;
    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr orange_publisher_;
    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr buzzer_publisher_;
    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr light_publisher_;

    // fan topics
    rclcpp::Publisher<std_msgs::msg::UInt8>::SharedPtr intake1_publisher_;
    rclcpp::Publisher<std_msgs::msg::UInt8>::SharedPtr intake2_publisher_;
    rclcpp::Publisher<std_msgs::msg::UInt8>::SharedPtr outake1_publisher_;
    rclcpp::Publisher<std_msgs::msg::UInt8>::SharedPtr outake2_publisher_;



    // motor topics
    rclcpp::Publisher<std_msgs::msg::Int8>::SharedPtr FR_velocity_publisher_;
    rclcpp::Publisher<std_msgs::msg::Int8>::SharedPtr FL_velocity_publisher_;
    rclcpp::Publisher<std_msgs::msg::Int8>::SharedPtr CR_velocity_publisher_;
    rclcpp::Publisher<std_msgs::msg::Int8>::SharedPtr CL_velocity_publisher_;
    rclcpp::Publisher<std_msgs::msg::Int8>::SharedPtr RR_velocity_publisher_;
    rclcpp::Publisher<std_msgs::msg::Int8>::SharedPtr RL_velocity_publisher_;

    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr right_velocity_subscriber_;
    rclcpp::Subscription<geometry_msgs::msg::Twist>::SharedPtr left_velocity_subscriber_;

};

int main(int argc, char* argv[]) {
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<Mega>());
    rclcpp::shutdown();
    return 0;
}
